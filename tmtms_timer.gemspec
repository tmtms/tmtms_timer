require_relative "lib/tmtms_timer/version"

Gem::Specification.new do |spec|
  spec.name = "tmtms_timer"
  spec.version = TmtmsTimer::VERSION
  spec.authors = ["TOMITA Masahiro"]
  spec.email = ["tommy@tmtm.org"]

  spec.summary = "Timer"
  spec.description = "Timer"
  spec.homepage = "https://gitlab.com/tmtms/tmtms_timer"
  spec.license = "MIT"
  spec.required_ruby_version = ">= 2.6.0"

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = spec.homepage
  spec.metadata["changelog_uri"] = "https://gitlab.com/tmtms/tmtms_timer/blob/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject do |f|
      (f == __FILE__) || f.match(%r{\A(?:(?:bin|test|spec|features)/|\.(?:git|travis|circleci)|appveyor)})
    end
  end
  spec.bindir = "exe"
  spec.executables = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]
  spec.metadata['rubygems_mfa_required'] = 'true'
end
