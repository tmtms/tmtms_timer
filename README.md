# Tmtms::Timer

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'tmtms_timer'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install tmtms-timer

## Usage

```ruby
require 'tmtms_timer'
timer = Tmtms::Timer.new
timer.set(10){puts "hoge"}
sleep 20
# puts "hoge" after 10 seconds
timer.at(Time.local(2022,7,18,12,34,56)){puts "fuga"}
sleep
# puts "fuga" at 2022-07-18 12:34:56
```

To cancel timer:

```ruby
job = timer.set(10){puts "hoge"}
job.cancel
```

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).
