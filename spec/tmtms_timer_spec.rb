# frozen_string_literal: true

RSpec.describe TmtmsTimer do
  it 'execute block after sec seconds' do
    timer = Tmtms::Timer.new
    flag = false
    timer.set(1){flag = true}
    sleep 0.5
    expect(flag).to eq false
    sleep 0.6
    expect(flag).to eq true
  end

  it 'can cancel timer' do
    timer = Tmtms::Timer.new
    flag = false
    job = timer.set(1){flag = true}
    sleep 0.5
    expect(job.cancel).to be_truthy
    sleep 0.6
    expect(flag).to eq false
  end

  it 'cacnel return nil after timer expired' do
    timer = Tmtms::Timer.new
    flag = false
    job = timer.set(0.5){flag = true}
    sleep 0.6
    expect(job.cancel).to be_falsey
    expect(flag).to eq true
  end

  it 'can repeat block' do
    timer = Tmtms::Timer.new
    cnt = 0
    job = timer.repeat(0.3){cnt += 1}
    sleep 2
    job.cancel
    expect(cnt).to eq 7
  end
end
