require 'io/wait'

module Tmtms
  # Usage
  # @example
  #   t = Tmtms::Timer.new
  #   t.set(1){puts "hoge"}
  #   sleep 5  # puts "hoge" after 1 second.
  class Timer
    # Timer job
    class Job
      attr_accessor :time
      attr_reader :repeat, :block

      # @param timer [Tmtms::Timer]
      # @param time [Time]
      # @param block [Proc]
      def initialize(timer:, time:, repeat: nil, block:)
        @timer, @time, @repeat, @block = timer, time, repeat, block
      end

      # cancel this job.
      # @return [self]
      def cancel
        @timer.cancel(self)
      end
    end

    def initialize
      @rfd, @wfd = IO.pipe
      @jobs = []  #=> [job, ...]
      @mutex = Mutex.new
      do_loop
    end

    # execute block after sec seconds.
    # @param sec [Numeric]
    # @param repeat [Numeric] interval seconds
    # @return [Tmtms::Timer::Job] timer job
    def set(sec, repeat: nil, &block)
      at(Time.now + sec, repeat: repeat, &block)
    end

    # execute block at the specified time.
    # @param time [Time]
    # @param repeat [Numeric] interval seconds
    # @return [Tmtms::Timer::Job] timer job
    def at(time, repeat: nil, &block)
      @mutex.synchronize do
        job = Job.new(timer: self, time: time, repeat: repeat, block: block)
        @jobs.push job
        @jobs.sort_by!(&:time)
        @wfd.syswrite('.')
        return job
      end
    end

    # repeat block every sec seconds.
    # @param sec [Numeric] interval seconds
    # @return [Tmtms::Timer::Job] timer job
    def repeat(sec, &block)
      at(Time.now, repeat: sec, &block)
    end

    # cancle the timer.
    # @param job [Tmtms::Timer::Job] timer job
    # @return [Tmtms::Timer::Job] if the job was available.
    # @return [nil] if the job was not available.
    def cancel(job)
      @mutex.synchronize do
        @jobs.delete(job)
      end
    end

    private

    def do_loop
      Thread.new do
        loop do
          tout = @mutex.synchronize do
            @jobs.empty? ? nil : @jobs[0].time - Time.now
          end
          tout = 0 if tout&.negative?
          if @rfd.wait_readable(tout)
            @rfd.sysread(1)
          end
          loop do
            job = nil
            block = @mutex.synchronize do
              if @jobs[0] && @jobs[0].time <= Time.now
                job = @jobs.shift
                job.block
              end
            end
            break unless block
            block.call
            if job.repeat
              @mutex.synchronize do
                job.time = Time.now + job.repeat
                @jobs.push job
                @jobs.sort_by!(&:time)
              end
            end
          end
        end
      end
    end
  end
end
